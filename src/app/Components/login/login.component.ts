import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';
import Validateform from 'src/app/Helpers/validateform';
import { AuthService } from 'src/app/Services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  type:string = 'password';
  eyeicon:string='fa-eye-slash'
  isText:boolean = false;
  loginform!:FormGroup;
  constructor(private fb :FormBuilder,private auth:AuthService,private router:Router,private toast :NgToastService) {}
  ngOnInit():void {

      this.loginform=this.fb.group({
        userName: ['', [Validators.required, Validators.minLength(5)]], 
        password: ['', [Validators.required, Validators.minLength(8)]] 

    })
  }
  hideshowpass(){
    this.isText = !this.isText;
    this.isText ?this.eyeicon = 'fa-eye' : this.eyeicon = 'fa-eye-slash';
    this.isText? this.type = 'text': this.type = 'password';
  
  }
  onLogin(){
    if(this.loginform.valid){   
      console.log(this.loginform.value)
      //Send obj to database
      this.auth.login(this.loginform.value)
      .subscribe({
        next:(res)=>{
          this.toast.success({detail:"SUCCESS",summary:res.message,duration:5000});   
          this.loginform.reset();
          this.router.navigate(['/dashboard']);
        },
        error:(err)=>{
          this.toast.error({detail:"ERROR",summary:"Login credentials are Wrong",duration:5000});
        }
      })
  }
  else{
    Validateform.validateAllFormFields(this.loginform);
  }
}


    }
  

