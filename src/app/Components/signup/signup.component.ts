import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Validateform from 'src/app/Helpers/validateform';
import { AuthService } from 'src/app/Services/auth.service';
Validateform
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent {
  type:string = 'password';
  eyeicon:string='fa-eye-slash'
  isText:boolean = false;
  signUpForm!: FormGroup;
  constructor(private fb:FormBuilder,private auth: AuthService,private router:Router){}
  ngOnInit():void{
    this.signUpForm=this.fb.group({
      firstName:['',Validators.required],
      lastName:['',Validators.required],
      email:['',Validators.required],
      userName:['',Validators.required],
      password:['',Validators.required],

    })
  }
  hideshowpass(){
    this.isText = !this.isText;
    this.isText ?this.eyeicon = 'fa-eye' : this.eyeicon = 'fa-eye-slash';
    this.isText? this.type = 'text': this.type = 'password';

  }
  onSignUp(){
    if(this.signUpForm.valid){
      console.log(this.signUpForm.value)
      //Send obj to database
      this.auth.signUp(this.signUpForm.value)
      .subscribe({
        next:(res=>{
        alert(res.message);
        this.signUpForm.reset();
        this.router.navigate(['login']);
      })
      ,error:(err=>{
          alert(err?.error.message)
      })
    })

    console.log(this.signUpForm.value)
   }
    else{
      Validateform.validateAllFormFields(this.signUpForm);
    }
  }
  


}
