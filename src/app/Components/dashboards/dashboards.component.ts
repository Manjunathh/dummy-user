import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboards',
  templateUrl: './dashboards.component.html',
  styleUrls: ['./dashboards.component.scss']
})
export class DashboardsComponent {
show: any;
  constructor (private router: Router) {}
  addNew(){
    this.router.navigate(['/form'])
  }

}
