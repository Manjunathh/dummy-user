import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../Services/auth.service';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent {
  userCreationForm: any;
  
  constructor(private router: Router,private formBuilder: FormBuilder,private auth:AuthService){}
  ngOnInit():void{
    this.userCreationForm=this.formBuilder.group({
      FIRSTNAME: ['', [
        Validators.required,
        RxwebValidators.required({ message: 'Invalid email format' }) 
      ]],
      LASTNAME:['',Validators.required],
      EMAIL: ['', [
        Validators.required,
        Validators.email,
        RxwebValidators.email({ message: 'Invalid email format' }) 
      ]],
      ADHARNO: ['', [
        Validators.required,
        RxwebValidators.digit(),
        RxwebValidators.maxNumber({ value: 12 }),
        RxwebValidators.minNumber({ value: 12 }),
        RxwebValidators.numeric({ message: 'Invalid Aadhar format' }) 
      ]],
      PHONE:[,Validators.required,
        RxwebValidators.maxNumber({ value: 12 }),
        RxwebValidators.minNumber({ value: 10 }),
        RxwebValidators.numeric({ message: 'Invalid Phone number format' }) 
      ],
      ADDRESSONE:['',Validators.required],
      STARTDATE:['',Validators.required],
      ENDDATE:['',Validators.required],
      MIDDLENAME:['',Validators.required],
      ADDRESSTWO:['',Validators.required],
      ROLE:['',Validators.required],
      USERNAME:['',],
      STATUS:['Active',Validators.required]

    })
  } 
  onSubmit(){ 
    console.log(this.userCreationForm.value);
    if(this.userCreationForm.valid){
     console.log(this.userCreationForm.value);
      this.auth.addUser(this.userCreationForm.value)
        .subscribe({
          next:(res)=>{
            alert(res.message)
            location.reload;
          },
          error:(err)=>alert(err?.errorMessage)
        })
        console.log("38",this.userCreationForm.value);
      this.router.navigate(['dashboard']);
     }
     else{
      console.log("Validation failed!");
     };
  }
  onCancel(){
    this.router.navigate(['dashboard'])
  }

}

