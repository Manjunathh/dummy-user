import { Component } from '@angular/core';
import { DataService } from '../Services/data.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatButtonToggle } from '@angular/material/button-toggle';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from '../bookmarkedusers/bookmarkedusers.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { AuthService } from '../Services/auth.service';

@Component({
  selector: 'app-allusers',
  templateUrl: './allusers.component.html',
  styleUrls: ['./allusers.component.scss']
})
export class AllusersComponent {
  bookmarked:boolean = true;
  menu:string="matMenu";
  digit:number=2;
  
  isChecked:boolean=true;
  users: User[] = [];
  displayedColumns=['Id','FirstName','LastName','UserName','Role','Email','StartDate','Status','Bookmark'];
  dataSource:any;
  selectedUsers: any[] = [];
  constructor(private data:DataService,private http:HttpClient,private auth:AuthService){}
  ngOnInit(){
    this.data.getAllUsers().subscribe((res:any)=>{
      this.dataSource=new MatTableDataSource(res);
      console.log(res[0].id);
    })
  }
  
  OnBook(rowId: number) {
    console.log(rowId);

    this.auth.book(rowId).subscribe({
      next: (res) => {
        console.log('API call successful:', res);
        console.log(res);
      },
      error: (err) => {
        console.error('Error calling API:', err);
        console.log(err);
      }
    });
  }  

  OnUnBook(rowId: number) {
    console.log(rowId);

    this.auth.unbook(rowId).subscribe({
      next: (res) => {
        console.log('API call successful:', res);
        console.log(res);
      },
      error: (err) => {
        console.error('Error calling API:', err);
        console.log(err);
      }
    });
  }  

  Active(id:number) {
    console.log(id);

    this.auth.active(id).subscribe({
      next: (res) => {
        console.log('API call successful:', res);
        console.log(res);
      },
      error: (err) => {
        console.error('Error calling API:', err);
        console.log(err);
      }
    });
  }  

  InActive(id: number) {
    console.log(id);

    this.auth.inactive(id).subscribe({
      next: (res) => {
        console.log('API call successful:', res);
        console.log(res);
      },
      error: (err) => {
        console.error('Error calling API:', err);
        console.log(err);
      }
    });
  }  

  
}
  
