import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../bookmarkedusers/bookmarkedusers.component';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private apiUrl = 'https://localhost:7286/api/User/';

  constructor(private http:HttpClient) { }
  getAllUsers(){
  return this.http.get('https://localhost:7286/api/User/Getallusers')
  }
  bookmarkUser(user: User): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/bookmarkUser`, user);
  }

  getAllBookmarkedUsers(){
    return this.http.get('https://localhost:7286/api/User/getBookmarkedUser')
    }

}
