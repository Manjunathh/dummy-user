import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private baseUrl: string="https://localhost:7286/api/User/"
  constructor(private http:HttpClient) { }
  signUp(userObj:any){
    return this.http.post<any>(`${this.baseUrl}register`, userObj);
  }
  login(loginObj:any){
    return this.http.post<any>(`${this.baseUrl}authenticate`, loginObj);
  }
  addUser(newuserobj:any){
    return this.http.post<any>(`${this.baseUrl}adduser`, newuserobj);
  }

  book( id:number){
    console.log(id)
    return this.http.post<any>(`${this.baseUrl}bookmarkUser?id=${id}`,{id});
  }

  unbook( id:number){
    console.log(id)
    return this.http.post<any>(`${this.baseUrl}unbookmarkUser?id=${id}`,{id});
  }
  active(id:number){
    return this.http.post<any>(`${this.baseUrl}active?id=${id}`,{id});

  }
  inactive(id:number){
    return this.http.post<any>(`${this.baseUrl}inactive?id=${id}`,{id});

  }
  
}


