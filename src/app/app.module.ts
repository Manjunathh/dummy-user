import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './Components/login/login.component';
import { SignupComponent } from './Components/signup/signup.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DashboardsComponent } from './Components/dashboards/dashboards.component';
import { NgToastComponent, NgToastModule } from 'ng-angular-popup';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormComponent } from './form/form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { AllusersComponent } from './allusers/allusers.component';
import { BookmarkedusersComponent } from './bookmarkedusers/bookmarkedusers.component';
import { MatTableModule } from '@angular/material/table';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
// import {provideNativeDateAdapter} from '@angular/material/core';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    DashboardsComponent,
    HeaderComponent,
    FormComponent,
    AllusersComponent,
    BookmarkedusersComponent,
  ],
  imports: [
    BrowserModule,
    MatTableModule,MatButtonModule,
    AppRoutingModule,MatDatepickerModule,MatInputModule,
     ReactiveFormsModule,
      HttpClientModule, NgToastModule, NgbModule, BrowserAnimationsModule, MatTableModule, MatMenuModule, MatTabsModule, MatSlideToggleModule,
       MatFormFieldModule,
       RxReactiveFormsModule


  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class AppModule { }
