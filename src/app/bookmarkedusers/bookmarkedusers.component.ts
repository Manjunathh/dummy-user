import { Component, OnInit } from '@angular/core';
import { DataService } from '../Services/data.service';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from '../Services/auth.service';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number; 
  symbol: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];
export interface User {
  id: number;
  name: string;
  bookmarked: boolean;
}

@Component({
  selector: 'app-bookmarkedusers',
  templateUrl: './bookmarkedusers.component.html',
  styleUrls: ['./bookmarkedusers.component.scss']
})

export class BookmarkedusersComponent implements OnInit {
  
  
  displayedColumns: string[] = ['Id','FirstName','LastName','UserName','Role','Email','StartDate','Status'];
  dataSource:any;
  currentPage = 1; // Current page number
  pageSize = 1; 
  constructor(private data:DataService,private auth:AuthService) {
    // Initialize your dataSource here
  // Initialize with empty array
   // Load initial data
  }
  ngOnInit(): void {
    this.data.getAllBookmarkedUsers().subscribe((res:any)=>{
      this.dataSource=new MatTableDataSource(res);
      console.log(res);
    })
    // this.loadMoreData();
  }

 // Assuming dataSource is an array of your data
  // displayedColumns: string[] = ['position', 'name', 'weight', 'symbol']; // Assuming displayedColumns is an array of column names
  Active(id:number) {
    console.log(id);

    this.auth.active(id).subscribe({
      next: (res) => {
        console.log('API call successful:', res);
        console.log(res);
      },
      error: (err) => {
        console.error('Error calling API:', err);
        console.log(err);
      }
    });
  }  

  InActive(id: number) {
    console.log(id);

    this.auth.inactive(id).subscribe({
      next: (res) => {
        console.log('API call successful:', res);
        console.log(res);
      },
      error: (err) => {
        console.error('Error calling API:', err);
        console.log(err);
      }
    });
  }  



  loadMoreData() {
    // Simulated method to load more data (replace with actual implementation)
    // For demonstration, let's add dummy data
    for (let i = 0; i < this.pageSize; i++) {
      const newItem = {
        position: this.dataSource.length + i + 1,
        name: `Item ${this.dataSource.length + i + 1}`,
        weight: Math.random() * 1000,
        symbol: `S${this.dataSource.length + i + 1}`
      };
      this.dataSource.push(newItem);
    }
  }
 
  onLoadMoreClick() {
    this.currentPage++;
    this.loadMoreData();

  }
}