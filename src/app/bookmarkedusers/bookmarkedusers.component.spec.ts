import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmarkedusersComponent } from './bookmarkedusers.component';

describe('BookmarkedusersComponent', () => {
  let component: BookmarkedusersComponent;
  let fixture: ComponentFixture<BookmarkedusersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BookmarkedusersComponent]
    });
    fixture = TestBed.createComponent(BookmarkedusersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
